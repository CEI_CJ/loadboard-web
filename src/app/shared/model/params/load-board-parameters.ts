export class LoadBoardParameters {
  equipmentId: number;
  loadId: number;
  destinationCsz: string;
  originCsz: string;
  customerId: number;
}
